import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';


class GestorEvento extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    render() { //parte visual del web component
        return html `
            <h3>Gestor Evento!</h3>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="reciever"></receptor-evento>
        `;
    }

    processEvent(e) {
        console.log("Se ha capturado el evento del emisor");
        console.log(e.detail);
        this.shadowRoot.getElementById("reciever").course=e.detail.course;
        this.shadowRoot.getElementById("reciever").year=e.detail.year;
    }
}

customElements.define('gestor-evento', GestorEvento);