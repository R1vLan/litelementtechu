import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {
                type: String,
            },
            yearsInCompany: {
                type: Number
            },
            photo: {
                type: Object,
            },
            personInfo: {
                type: String
            },
        };
    }

    constructor() {
        super();
        this.name = "Prueba nombre";
        this.yearsInCompany = 1;
        this.photo = {
            src: "./img/persona.jpg",
            alt: "Foto Persona"
        };
        this.updatePersonInfo();
    }

    render() { //parte visual del web component
        return html `
            <div>Ficha Persona</div>
            <div>
                <label for="fname">Nombre Completo</>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br/>

                <label for="yearsInCompany">Años en la compañia</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/>

                <input type="text" name="personInfo" value="${this.personInfo}" disabled /> 
                <br/>

                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo}"></img>
            </div>
        `;
    }

    //@input un listener o escuchador que esta viendo los cambios que sucedan ahí mismo

    //parte del ciclo de vida de LitElement
    //propiedad de los callback de litelement, al parecer solo al cargar y espera al updateName porque ahi recien cambia
    updated(changedProperties) {
        if(changedProperties.has("name")) {
            console.log("Propiedad name cambio valor de: " + changedProperties.get("name") + " a: " + this.name);
        }

        if(changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambio valor de: " + changedProperties.get("yearsInCompany") + " a: " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    //se asigna valor cada vez que hay un cambio al input
    updateName(e) {
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("update yearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        console.log("yearsInCompany vale: " + this.yearsInCompany);

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >=5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >=3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
}

customElements.define('ficha-persona', FichaPersona);