import { LitElement, html } from 'lit-element';

class EmisorEvento extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    render() { //parte visual del web component
        return html `
            <h3>Emisor Evento!</h3>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }

    sendEvent(e) {
        console.log("Se ha pulsado el boton");
        console.log(e);

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail": {
                        "course": "TechU",
                        "year": 2020
                    }
                }
            )
        )
    }
}

customElements.define('emisor-evento', EmisorEvento);