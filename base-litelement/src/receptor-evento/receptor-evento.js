import { LitElement, html } from 'lit-element';

class ReceptorEvento extends LitElement {

    static get properties() {
        return {
            course: {
                type: String
            },
            year: {
                type: Number
            }
        };
    }

    constructor(){
        super();
    }

    render() { //parte visual del web component
        return html `
            <h3>Receptor evento</h3>
            <h5>Este curso es de: ${this.course}</h5>
            <h5>Estamos en el año: ${this.year}</h5>
        `;
    }
}

customElements.define('receptor-evento', ReceptorEvento);