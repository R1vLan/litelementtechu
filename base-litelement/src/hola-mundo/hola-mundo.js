import { LitElement, html } from 'lit-element';

class HolaMundo extends LitElement {
    render() { //parte visual del web component
        return html `
            <div>Hola Mundo!</div>
        `;
    }
}

customElements.define('hola-mundo', HolaMundo);