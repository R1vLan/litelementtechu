import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: {
                type: Array
            }
        };
    }

    constructor(){
        super();
        this.movies = [];
        this.getMovieData();
    }
    render() { //parte visual del web component
        return html `
            ${this.movies.map(
                movie => 
                    html `
                        <div>La pelicula ${movie.title}, fue dirigida por ${movie.director}</div>
                    `
            )}
        `;
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de los films de SW");

        // para hacer peticion AJAX
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                //si fuese objeto sería cambiar el formato o algo así
                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/");
        xhr.send();
    }
}

customElements.define('test-api', TestApi);